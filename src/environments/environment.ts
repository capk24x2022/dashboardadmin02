// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyASTVSfIKIZD_0LdhZawteoxoL81hn9u1g",
    authDomain: "medversemobileapp.firebaseapp.com",
    databaseURL: "https://medversemobileapp-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "medversemobileapp",
    storageBucket: "medversemobileapp.appspot.com",
    messagingSenderId: "63545294352",
    appId: "1:63545294352:web:d457af3c724ae27e6e5495",
    measurementId: "G-KVH1VSMK5V"

  },
  baseUrl: 'http://medverse.ddns.net/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
