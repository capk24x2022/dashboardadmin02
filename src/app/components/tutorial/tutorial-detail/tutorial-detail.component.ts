import { Component, OnInit } from '@angular/core';
import {ProductService} from "../../../services/product.service";
import {Tutorial} from "../../../models/tutorial.model";
import {Product} from "../../../models/product.model";
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tutorial-detail',
  templateUrl: './tutorial-detail.component.html',
  styleUrls: ['./tutorial-detail.component.scss']
})
export class TutorialDetailComponent implements OnInit {
  currentTutorial: Product = {};
  id: any;
  constructor(private productService: ProductService,  private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if(this.id) {
        this.getTutorial(1);
      }

    });

  }
  getTutorial(id: any): void {
    this.productService.get(id)
      .subscribe({
        next: (data) => {
          this.currentTutorial = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
}
