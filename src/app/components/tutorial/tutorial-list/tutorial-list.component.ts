import {Component, OnInit, ViewChild} from '@angular/core';
import {TutorialService} from "../../../services/tutorial.service";
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../models/product.model";

import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-tutorial-list',
  templateUrl: './tutorial-list.component.html',
  styleUrls: ['./tutorial-list.component.scss']
})
export class TutorialListComponent implements OnInit {


  tutorials?: Product[];
  productsClone?: Product[];
  currentTutorial: Product = {};
  currentIndex = -1;
  title = '';
  textSearch = '';
  confirmDelete = '';
  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<string[]> | undefined;

  public displayedColumnsText: string[] = ['Product ID', 'Drugbank ID', 'Product Name', 'Product Labeller', 'Product Code'];
  displayedColumns: string[] = ['productID',  'drugbankID', 'productName', 'productLabeller', 'productCode'];
  public columnsToDisplay: string[] = [...this.displayedColumns, 'actions'];
  dataSource = new MatTableDataSource<Product>();


  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  ngAfterViewInit() {
    // @ts-ignore
    this.dataSource.paginator = this.paginator;
  }

  constructor(private tutorialService: TutorialService,
              private productService: ProductService) { }

  ngOnInit(): void {
    // an example array of 150 items to be paged


    this.retrieveTutorials();
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
  }


  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    // @ts-ignore
    return this.productsClone.filter(option => option.productName.toLowerCase().includes(filterValue));
  }
  retrieveTutorials(): void {
    this.productService.getAll()
      .subscribe({
        next: (data) => {
          this.tutorials = data;
          this.dataSource.data = data;
          this.productsClone = data;
          console.log(data);
        },
        error: (e) => console.error(e)
      });
  }
  refreshList(): void {
    this.retrieveTutorials();
    this.currentTutorial = {};
    this.currentIndex = -1;
  }
  setActiveTutorial(tutorial: Product, index: number): void {
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }
  removeAllTutorials(): void {
    this.productService.deleteAll()
      .subscribe({
        next: (res) => {
          console.log(res);
          this.refreshList();
        },
        error: (e) => console.error(e)
      });
  }

  edit(item: any) {

  }
  delete(item: any) {

  }

  filter(): void {
    console.log(this.textSearch);
    if(this.myControl.value) {
      this.tutorials = this.productsClone?.filter((item: Product) => item?.productName?.toLowerCase().includes(this.myControl.value.toLowerCase()));
    } else {
      this.refreshList();
    }
  }
}
