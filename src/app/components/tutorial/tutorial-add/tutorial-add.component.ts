import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {TutorialService} from "../../../services/tutorial.service";
import {first} from "rxjs";
import {ProductService} from "../../../services/product.service";

@Component({
  selector: 'app-tutorial-add',
  templateUrl: './tutorial-add.component.html',
  styleUrls: ['./tutorial-add.component.scss']
})
export class TutorialAddComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  submitted = false;
  id: any;
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private productService: ProductService) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      drugbankID: ['', Validators.required],
      productName: ['', Validators.required],
      productLabeller: ['', Validators.required],
      productCode: ['', [Validators.required]],
    });
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if(this.id) {
        this.getTutorial(1);
      }

    });
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    if(this.id) {
      this.updateUser();
    } else {
      this.createUser();
    }

    // if (this.isAddMode) {
    //   this.createUser();
    // } else {
    //   this.updateUser();
    // }
  }
  getTutorial(id: any): void {
    this.productService.get(id)
      .subscribe({
        next: (data) => {
          this.form.patchValue(data);
          console.log(data);
        },
        error: (e) => console.log(e)
      });
  }
  private createUser() {
    this.productService.create(this.form.value)
      .pipe(first())
      .subscribe((res) => {
        // this.alertService.success('User added', { keepAfterRouteChange: true });
        console.log('dasdsad');
        this.router.navigate(['admin/tutorials']);
      })
      .add(() => this.loading = false);
  }

  private updateUser() {
    this.router.navigate(['admin/tutorials']);
    // this.userService.update(this.id, this.form.value)
    //   .pipe(first())
    //   .subscribe(() => {
    //     this.alertService.success('User updated', { keepAfterRouteChange: true });
    //     this.router.navigate(['../../'], { relativeTo: this.route });
    //   })
    //   .add(() => this.loading = false);
  }

}
