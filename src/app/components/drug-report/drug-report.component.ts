import { Component, OnInit, ViewChild } from '@angular/core';
import { Report } from 'src/app/models/report.model';
import { Tutorial } from 'src/app/models/tutorial.model';
import { MatTableDataSource } from "@angular/material/table";
import { MatPaginator } from "@angular/material/paginator";
import { TutorialService } from '../../services/tutorial.service';
import { ReportService } from '../../services/report.service';


@Component({
  selector: 'app-drug-report',
  templateUrl: './drug-report.component.html',
  styleUrls: ['./drug-report.component.scss']
})
export class DrugReportComponent implements OnInit {
  currentId = '';
  blogs?: Report[];
  currentTutorial: Tutorial = {};
  currentIndex = -1;
  title = '';
  textSearch = '';

  public displayedColumnsText: string[] = ['ID','Name', 'Picture', 'Description', 'Created date'];
  displayedColumns: string[] = ['id','username',  'captureUrl', 'description', 'timestamp'];
  public columnsToDisplay: string[] = [...this.displayedColumns, 'actions'];
  dataSource = new MatTableDataSource<Report>();


  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  ngAfterViewInit() {
    // @ts-ignore
    this.dataSource.paginator = this.paginator;
  }


  constructor(private tutorialService: TutorialService,
              private reportService: ReportService) { }

  ngOnInit(): void {
    this.retrieveTutorials();
  }
  retrieveTutorials(): void {
    this.reportService.getAll().subscribe(data => {
      console.log(data);
      this.dataSource.data = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as Report
        } as Report;
      })

    });
  }


  refreshList(): void {
    this.retrieveTutorials();
    this.currentTutorial = {};
    this.currentIndex = -1;
  }
  setActiveTutorial(tutorial: Tutorial, index: number): void {
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }
  removeReport(): void {
    this.reportService.delete(this.currentId)
      .then(data => {
        this.refreshList();
      });
  }

  edit(item: any) {

  }

  delete(id: any) {
    this.currentId = id;
  }

}

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
