import { Component, OnInit } from '@angular/core';
import {ChartOptions, ChartType} from "chart.js";
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { UserService } from '../../services/user.service';
import { BlogService } from 'src/app/services/blog.service';
import { ProductService } from 'src/app/services/product.service';
@Component({
  selector: 'app-dashboard2',
  templateUrl:'./dashboard2.component.html',
  styleUrls: ['./dashboard2.component.scss']
})
export class Dashboard2Component implements OnInit {
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['Thuốc kháng sinh'], ['Thuốc Ho'], ['Thuốc giảm đau'], 'Đông y'];
  public pieChartData: SingleDataSet = [300, 500, 100, 100];
  public pieChartType: ChartType = 'doughnut';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  totalBlog = 0;
  totalUser = 0;
  totalProduct = 0;


  constructor(private blogService: BlogService, private userService:UserService, private productService:ProductService) {

  }

  ngOnInit(): void {
    this.blogService.getAll().subscribe(data => {
      this.totalBlog = data.length;

    this.userService.getAll().subscribe(data =>
      this.totalUser = data.length
    )



    });
    this.productService.getAll().subscribe(data =>
      this.totalProduct = data.length)
  }



}




