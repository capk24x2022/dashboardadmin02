import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../../services/product.service";
import {first, Observable} from "rxjs";
import {BlogService} from "../../../services/blog.service";
import {AngularFireStorage} from "@angular/fire/compat/storage";
import { catchError, finalize } from 'rxjs/operators';
@Component({
  selector: 'app-blog-add',
  templateUrl: './blog-add.component.html',
  styleUrls: ['./blog-add.component.scss']
})
export class BlogAddComponent implements OnInit {
  form!: FormGroup;
  loading = false;
  submitted = false;
  id: any;
  title = "cloudsSorage";
  selectedFile: File | undefined;
  fb: any;
  downloadURL: Observable<string> | undefined;
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private blogService: BlogService,
              private storage: AngularFireStorage) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      id: [''],
      createdAt: [''],
      description: [''],
      status: [''],
      mediaUrl: ['']
    });
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if(this.id) {
        this.getTutorial(this.id);
      }
    });
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    console.log(this.form.invalid);
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    if(this.id) {
      this.updateUser();
    } else {
      this.createUser();
    }

    // if (this.isAddMode) {
    //   this.createUser();
    // } else {
    //   this.updateUser();
    // }
  }
  getTutorial(id: any): void {

    this.blogService.get(id).subscribe((snapshot) => {
      const data = snapshot.data();
      console.log(data);
      if (!data) {
      } else {
        // @ts-ignore

        this.form.patchValue(data);
        this.form?.get('id')?.setValue(id);
      }
    });
    // this.blogService.get(id)
    //   .subscribe({
    //     next: (data) => {
    //
    //       console.log(data);
    //     },
    //     error: (e) => console.log(e)
    //   });
  }
  private createUser() {
    this.blogService.create(this.form.value)
      .pipe(first())
      .subscribe((res) => {
        // this.alertService.success('User added', { keepAfterRouteChange: true });
        console.log('dasdsad');
        this.router.navigate(['admin/blogs']);
      })
      .add(() => this.loading = false);
  }

  private updateUser() {
    this.blogService.update(this.id, this.form.value)
      .then(() => this.router.navigate(['admin/blogs']))
      .catch(err => console.log(err));
    // this.router.navigate(['admin/blogs']);
    // this.userService.update(this.id, this.form.value)
    //   .pipe(first())
    //   .subscribe(() => {
    //     this.alertService.success('User updated', { keepAfterRouteChange: true });
    //     this.router.navigate(['../../'], { relativeTo: this.route });
    //   })
    //   .add(() => this.loading = false);
  }

  onFileSelected(event: any) {
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `RoomsImages/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`RoomsImages/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.fb = url;
              this.form.get('mediaUrl')?.setValue(url);
            }
            console.log(this.fb);

          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);

        }
      });
  }

}
