import {Component, OnInit, ViewChild} from '@angular/core';
import {Tutorial} from "../../models/tutorial.model";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {TutorialService} from "../../services/tutorial.service";
import {UserService} from "../../services/user.service";
import {User} from "../../shared/services/user";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  currentId = '';
  users?: User[];
  currentTutorial: Tutorial = {};
  currentIndex = -1;
  title = '';
  textSearch = '';

  public displayedColumnsText: string[] = ['Avatar', 'ID', 'Email', 'Name'];
  displayedColumns: string[] = ['photoUrl','id',  'email', 'username'];
  public columnsToDisplay: string[] = [...this.displayedColumns, 'actions'];
  dataSource = new MatTableDataSource<User>();


  @ViewChild(MatPaginator) paginator: MatPaginator | undefined;

  ngAfterViewInit() {
    // @ts-ignore
    this.dataSource.paginator = this.paginator;
  }

  constructor(private tutorialService: TutorialService,
              private userService: UserService) {

  }


  ngOnInit(): void {

    this.retrieveTutorials();

  }
  retrieveTutorials(): void {
    this.userService.getAll().subscribe(data => {
      console.log(data);
      this.dataSource.data = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as User
        } as User;
      })

    });
  }


  refreshList(): void {
    this.retrieveTutorials();
    this.currentTutorial = {};
    this.currentIndex = -1;
  }
  setActiveTutorial(tutorial: Tutorial, index: number): void {
    this.currentTutorial = tutorial;
    this.currentIndex = index;
  }
  remove(): void {
    this.userService.delete(this.currentId)
      .then(data => {
        this.refreshList();
      });
  }

  edit(item: any) {
    
  }

  delete(id: any) {
    this.currentId = id;
  }

}
