import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first, Observable} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {BlogService} from "../../../services/blog.service";
import {AngularFireStorage} from "@angular/fire/compat/storage";
import {finalize} from "rxjs/operators";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  form!: FormGroup;
  loading = false;
  submitted = false;
  id: any;
  title = "cloudsSorage";
  selectedFile: File | undefined;
  fb: any;
  downloadURL: Observable<string> | undefined;
  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private blogService: BlogService,
              private userService: UserService,
              private storage: AngularFireStorage) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      username: [''],
      photoUrl: [''],
      uid:['']
    });
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      if(this.id) {
        this.getUser(this.id);
      }

    });
  }

  get f() { return this.form.controls; }

  onSubmit() {
    this.submitted = true;


    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.loading = true;
    if(this.id) {
      this.updateUser();
    } else {
      this.createUser();
    }

    // if (this.isAddMode) {
    //   this.createUser();
    // } else {
    //   this.updateUser();
    // }
  }
  getUser(id: any): void {

    this.userService.get(id).subscribe((snapshot) => {
      const data = snapshot.data();
      console.log(data);
      if (!data) {
      } else {
        // @ts-ignore
        this.form.patchValue(data);
      }
    });
    // this.blogService.get(id)
    //   .subscribe({
    //     next: (data) => {
    //
    //       console.log(data);
    //     },
    //     error: (e) => console.log(e)
    //   });
  }
  private createUser() {
    this.blogService.create(this.form.value)
      .pipe(first())
      .subscribe((res) => {
        // this.alertService.success('User added', { keepAfterRouteChange: true });
        console.log('dasdsad');
        this.router.navigate(['admin/users']);
      })
      .add(() => this.loading = false);
  }

  private updateUser() {
    this.userService.update(this.id, this.form.value)
      .then(() => this.router.navigate(['admin/users']))
      .catch(err => console.log(err));
    // this.router.navigate(['admin/blogs']);
    // this.userService.update(this.id, this.form.value)
    //   .pipe(first())
    //   .subscribe(() => {
    //     this.alertService.success('User updated', { keepAfterRouteChange: true });
    //     this.router.navigate(['../../'], { relativeTo: this.route });
    //   })
    //   .add(() => this.loading = false);
  }

  onFileSelected(event: any) {
    var n = Date.now();
    const file = event.target.files[0];
    const filePath = `RoomsImages/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`RoomsImages/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.fb = url;
              this.form.get('photoUrl')?.setValue(url);
            }
            console.log(this.fb);

          });
        })
      )
      .subscribe(url => {
        if (url) {
          console.log(url);

        }
      });
  }


}
