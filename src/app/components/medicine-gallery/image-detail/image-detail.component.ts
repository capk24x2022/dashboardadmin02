import {Component, Inject, OnInit} from '@angular/core';
import {ImageService} from "../../../services/image.service";
import {ActivatedRoute} from "@angular/router";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ConfirmDialogComponent} from "../../../dialogs/confirm-dialog/confirm-dialog.component";
import {AngularFireStorage} from "@angular/fire/compat/storage";


@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.component.html',
  styleUrls: ['./image-detail.component.scss']
})
export class ImageDetailComponent implements OnInit {

  image:any;
  fullPath: string = '';
  constructor(private imageService: ImageService,
              public dialog: MatDialog,
              private route: ActivatedRoute,
              private storage: AngularFireStorage,
              @Inject(MAT_DIALOG_DATA) public data: {id: any, fullPath: string},
              public imageDetailDialog: MatDialogRef<ImageDetailComponent>) { }

  ngOnInit(){
    this.image = this.data.id;
    this.fullPath = this.data.fullPath;
  }
  confirmDelete() {
    let dialogRef =  this.dialog.open(ConfirmDialogComponent, {

    });
    dialogRef.afterClosed().subscribe(result => {
      if(result === 'yes') {
        this.storage.ref(this.fullPath).delete().subscribe(result => {
          alert('delete success file : ' + this.fullPath)
        })
      }
      this.imageDetailDialog.close();
    });
  }
}
