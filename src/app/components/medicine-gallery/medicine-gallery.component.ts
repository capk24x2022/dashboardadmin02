import {Component, OnInit} from '@angular/core';
import {ImageService} from "../../services/image.service";
import {FileUploadService} from "../../services/file-upload.service";
import {MatDialog} from "@angular/material/dialog";
import {ImageDetailComponent} from "./image-detail/image-detail.component";
import {AngularFireStorage} from "@angular/fire/compat/storage";
import {FileService} from "../../services/file.service";


@Component({
  selector: 'app-medicine-gallery',
  templateUrl: './medicine-gallery.component.html',
  styleUrls: ['./medicine-gallery.component.scss']
})
export class MedicineGalleryComponent implements OnInit {
  images: any[] = [];
  filterBy?: string = 'all'
  allImages: any[] = [];
  fileUploads?: any[];
  filelist = [{
    name: '',
    link: '',
    fullPath: ''
  }];
  listImage = [];
  firstPage: any = {};
  nextPageToken = '';

  constructor(private imageService: ImageService,
              private uploadService: FileUploadService,
              public dialog: MatDialog,
              private storage: AngularFireStorage,
              private fileService: FileService) {
  }

  ngOnInit() {
    this.getFileList();
    // console.log('filelist', this.filelist);
    // this.allImages = this.imageService.getImages();
    // console.log(this.fileService.getImage('1.png'));
  }

  getFileList() {
    this.filelist = [];

    // const ref = this.storage.ref('/uploads');
    var firstpage = this.storage.ref('/pill_images/image/images/gallery/300').list({maxResults: 102});
    this.firstPage = firstpage;
    let myurlsubscription = firstpage.subscribe((data: any) => {

      this.nextPageToken = data.nextPageToken;
      for (let i = 0; i < data.items.length; i++) {
        let name = data.items[i].name;
        let fullPath = data.items[i].fullPath;
        let newref = this.storage.ref(fullPath);
        let url = newref.getDownloadURL().subscribe((data) => {

          this.filelist.push({
            name: name,
            link: data,
            fullPath: fullPath
          });
        });
      }


    });
  }

  nextPage() {
    // console.log(this.storage.ref('/pill_images/image/images/gallery/300').list({ maxResults : 30}).ne);
    if (this.nextPageToken) {
      this.filelist = [];
      const secondPage = this.storage.ref('/pill_images/image/images/gallery/300').list({
        maxResults: 102,
        pageToken: this.nextPageToken
      }).subscribe((data: any) => {
        for (let i = 0; i < data.items.length; i++) {
          let name = data.items[i].name;
          let newref = this.storage.ref(data.items[i].fullPath);
          let url = newref.getDownloadURL().subscribe((data) => {
            this.filelist.push({
              name: name,
              link: data,
              fullPath: data.items[i].fullPath
            });
          });
        }


      });
    }
  }

  ngOnChanges() {
    // this.service.imageDetailList.snapshotChanges().subscribe(
    //   list => {
    //     this.allImages = list.map(item => { return item.payload.val(); });
    //     console.log(this.allImages);
    //   }
    // );
    this.allImages = this.imageService.getImages();
  }

  openImageDialog(id: any, fullPath: string) {
    const dialog = this.dialog.open(ImageDetailComponent, {
      data: {
        id: id,
        fullPath: fullPath
      },
    });
    dialog.afterClosed().subscribe(result => {
      this.getFileList();
    })
  }

}
