import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../shared/services/auth.service';
import {User} from "../../shared/services/user";
import {Router} from "@angular/router";
import {FormControl, Validators} from "@angular/forms";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
})

export class SignInComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  hide = true;
  exist = false;

  password = new FormControl('', [Validators.required]);

  constructor(public authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {

    if (localStorage.getItem('user')) {
      this.router.navigate(['/admin/home']);
    }

    this.email.valueChanges.subscribe(value => {
      this.authService.checkExist(value).then(signInMethods => {
        if (signInMethods.length > 0) {
          this.exist = true;
        } else {
          this.exist = false;
        }
      });
    })
  }

  login(): void {
    console.log('1234', this.email, this.password);
    if (this.email.valid && this.password.value) {
      console.log(this.email.value, this.password.value);
      this.authService.SignIn(this.email.value, this.password.value);
    }

  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      this.exist = false;
      return 'Email is required';
    }
    if (this.email.hasError('email')) {
      this.exist = false;
      return 'Invalid email';
    }
    return '';
  }

  getErrorMessage2() {
    if (this.password.hasError('required')) {
      return 'Password is required';
    }

    return '';
  }

  resetPassword() {
    this.router.navigate(['/reset-password']);
  }
}
