import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {AuthService} from "../../shared/services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  hide = true;
  exist = false;

  constructor(public authService: AuthService,
              private router: Router) {
  }

  ngOnInit() {

    this.email.valueChanges.subscribe(value => {
      this.authService.checkExist(value).then(signInMethods => {
        if (signInMethods.length > 0) {
          this.exist = true;
        } else {
          this.exist = false;
        }
      });
    })
  }

  reset(): void {
    console.log('1234', this.email);
    if (this.email.valid) {
      console.log(this.email.value,);
      this.authService.ForgotPassword(this.email.value);
    }

  }

  getErrorMessage() {
    if (this.email.hasError('required')) {
      this.exist = false;
      return 'Email is required';
    }
    if (this.email.hasError('email')) {
      this.exist = false;
      return 'Invalid email';
    }
    return '';
  }

  getErrorMessage2() {


    return '';
  }
}


