import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireStorageModule } from '@angular/fire/compat/storage';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { environment } from '../environments/environment';

// components
import { DashboardComponent } from './components/layout/dashboard.component';
import { SignInComponent } from './components/sign-in/sign-in.component';


// routing
import { AppRoutingModule } from './app-routing.module';

// service
import { AuthService } from './shared/services/auth.service';
import { HomePageComponent } from './components/home-page/home-page.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TutorialListComponent } from './components/tutorial/tutorial-list/tutorial-list.component';
import { TutorialDetailComponent } from './components/tutorial/tutorial-detail/tutorial-detail.component';
import { TutorialAddComponent } from './components/tutorial/tutorial-add/tutorial-add.component';
import { HttpClientModule } from "@angular/common/http";
import { Dashboard2Component } from './components/dashboard2/dashboard2.component';
import { ChartsModule } from "ng2-charts";
import { TutorialUpdateComponent } from "./components/tutorial/tutorial-update/tutorial-update.component";
import { BlogListComponent } from './components/blog/blog-list/blog-list.component';
import { BlogUpdateComponent } from './components/blog/blog-update/blog-update.component';
import { BlogAddComponent } from './components/blog/blog-add/blog-add.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatNativeDateModule } from "@angular/material/core";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { MatInputModule } from "@angular/material/input";
import { MatIconModule } from "@angular/material/icon";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { MatButtonModule} from "@angular/material/button";
import { UsersComponent } from './components/users/users.component';
import { UpdateComponent } from './components/users/update/update.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { DataInteractionComponent } from './components/Data-Interaction/Data-Interaction.component';
import { MedicineGalleryComponent } from './components/medicine-gallery/medicine-gallery.component';
import { GalleryComponent } from './components/medicine-gallery/gallery/gallery.component';
import { ImageComponent } from './components/medicine-gallery/image/image.component';
import { UploaderComponent } from './components/medicine-gallery/uploader/uploader.component';
import { ImageFilterPipe } from './components/medicine-gallery/image/shared/filter.pipe';
import { ImageService } from './components/medicine-gallery/image/shared/image.service';
import { DrugReportComponent } from './components/drug-report/drug-report.component';
import { UploadFormComponent } from './components/medicine-gallery/uploader/upload-form/upload-form.component';
import { UploadListComponent } from './components/medicine-gallery/uploader/upload-list/upload-list.component';
import { UploadDetailsComponent } from './components/medicine-gallery/uploader/upload-details/upload-details.component';
import { FilterimagesPipe } from './filterimages.pipe';
import {ImageDetailComponent} from "./components/medicine-gallery/image-detail/image-detail.component";
import {MatDialogModule} from "@angular/material/dialog";



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DataInteractionComponent,
    MedicineGalleryComponent,
    GalleryComponent,
    ImageComponent,
    UploaderComponent,
    SignInComponent,
    HomePageComponent,
    TutorialListComponent,
    TutorialDetailComponent,
    TutorialAddComponent,
    Dashboard2Component,
    TutorialUpdateComponent,
    BlogListComponent,
    BlogUpdateComponent,
    BlogAddComponent,
    ConfirmDialogComponent,
    UsersComponent,
    UpdateComponent,
    ResetPasswordComponent,
    ImageFilterPipe,
    DrugReportComponent,
    UploadFormComponent,
    UploadListComponent,
    UploadDetailsComponent,
    ImageDetailComponent,
    FilterimagesPipe

  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ChartsModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatInputModule,
    MatDialogModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    NgxPaginationModule,
    MatButtonModule
  ],
  providers: [AuthService, ImageService, ImageFilterPipe],
  bootstrap: [AppComponent],
})

export class AppModule { }
