import { BlogAddComponent } from './components/blog/blog-add/blog-add.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SignInComponent} from './components/sign-in/sign-in.component';
import {DashboardComponent} from './components/layout/dashboard.component';


// route guard
import {AuthGuard} from './shared/guard/auth.guard';
import {HomePageComponent} from "./components/home-page/home-page.component";
import {TutorialListComponent} from "./components/tutorial/tutorial-list/tutorial-list.component";
import {TutorialAddComponent} from "./components/tutorial/tutorial-add/tutorial-add.component";
import {TutorialDetailComponent} from "./components/tutorial/tutorial-detail/tutorial-detail.component";
import {Dashboard2Component} from "./components/dashboard2/dashboard2.component";
import {BlogListComponent} from "./components/blog/blog-list/blog-list.component";
import {BlogUpdateComponent} from "./components/blog/blog-update/blog-update.component";
import {UsersComponent} from "./components/users/users.component";
import {UpdateComponent} from "./components/users/update/update.component";
import {ResetPasswordComponent} from "./components/reset-password/reset-password.component";

import { DataInteractionComponent } from './components/Data-Interaction/Data-Interaction.component';
import { MedicineGalleryComponent } from './components/medicine-gallery/medicine-gallery.component';

import { DrugReportComponent } from './components/drug-report/drug-report.component';
import { UploaderComponent } from './components/medicine-gallery/uploader/uploader.component';
import {ImageDetailComponent} from "./components/medicine-gallery/image-detail/image-detail.component";

const routes: Routes = [
  {path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  {path: 'sign-in', component: SignInComponent},
  {
    path: 'image/:id', // child route path
    component: ImageDetailComponent, // child route component that the router renders
    canActivate: [AuthGuard]
  },
  {path: 'reset-password', component: ResetPasswordComponent},
  {
    path: 'admin', component: DashboardComponent,
    children: [
      {
        path: 'home', // child route path
        component: Dashboard2Component, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      // {
      //   path: 'statistics', // child route path
      //   component: Dashboard1Component, // child route component that the router renders
      //   canActivate: [AuthGuard]
      // },

      {
        path: 'data', // child route path
        component: DataInteractionComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine-gallery', // child route path
        component: MedicineGalleryComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },

      {
        path: 'uploader', // child route path
        component: UploaderComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine', // child route path
        component: TutorialListComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine/:id', // child route path
        component: TutorialDetailComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine-add', // child route path
        component: TutorialAddComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine-add/:id', // child route path
        component: TutorialAddComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'medicine/:id', // child route path
        component: TutorialDetailComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'user', // child route path
        component: HomePageComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'report', // child route path
        component: DrugReportComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'blogs', // child route path
        component: BlogListComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },

      {
        path: 'blogs-update/:id', // child route path
        component: BlogUpdateComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'blogs-add/:id', // child route path
        component: BlogAddComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'blogs-add', // child route path
        component: BlogAddComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'users', // child route path
        component: UsersComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
      {
        path: 'users/:id', // child route path
        component: UpdateComponent, // child route component that the router renders
        canActivate: [AuthGuard]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}
