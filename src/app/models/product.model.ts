export class Product {
  productID?: string;
  drugbankID?: string;
  productName?: string;
  productLabeller?: string;
  productCode?: string;
  productRoute?: string;
  productStrength?: string;
  productdosage?: string;
  approved?: string;
  otc?: string;
  generic?: string;
  country?: string;
}
