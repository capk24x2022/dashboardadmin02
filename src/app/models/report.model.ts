export class Report{
  captureId?: string
  captureUrel?: string
  description?: string
  id?: string
  ownerId?: string
  timestamp?: string
  username?: string
}
