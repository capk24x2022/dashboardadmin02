export class Blog {
  id?: string;
  title?: string;
  description?: string;
  mediaUrl?: string;
  status?:string;
  createdBy?: string;
  createdAt?: any;
}
