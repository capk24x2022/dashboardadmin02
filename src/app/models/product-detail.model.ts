export class ProductDetail {
  drugbank_ID?: string;
  drugName?: string;
  drugDescription?: string;
  drugState?: string;
  drugIndication?: string;
  drugPharmaco?: string;
  drugMechan?: string;
  drugToxicity?: string;
  drugMetabolism?: string;
  drugHalflife?: string;
  drugElimination?: string;
  drugClearance?: string;
}
