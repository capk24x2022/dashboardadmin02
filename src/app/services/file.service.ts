import {Inject, Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from "@angular/fire/compat/database";

@Injectable({
  providedIn: 'root'
})
export class FileService {
  imageDetailList: AngularFireList<any>;
  fileList: any[] | undefined;
  dataSet: Data = {
    id:'',
    url:''
  };
  msg:string = 'error';
  constructor(@Inject(AngularFireDatabase) private firebase: AngularFireDatabase) {
    this.imageDetailList = this.firebase.list('/profilePic');
  }
  getImageDetailList() {

  }
  insertImageDetails(id: string, url: string) {
    this.dataSet = {
      id : id,
      url: url
    };
    this.imageDetailList.push(this.dataSet);
  }
  getImage(value: string){
    this.imageDetailList.snapshotChanges().subscribe(
      list => {
        this.fileList = list.map(item => { return item.payload.val();  });
        console.log(this.fileList);
        this.fileList.forEach(element => {
          if(element.id===value)
            this.msg = element.url;
        });
        if(this.msg==='error')
          alert('No record found');
        else{
          window.open(this.msg);
          this.msg = 'error';
        }
      }
    );
  }
}
export interface Data{
  id:string;
  url:string;
}
