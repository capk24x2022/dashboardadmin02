import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from "@angular/fire/compat/database";
import {FileUpload} from "../models/file-upload.model";

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  allImages = [];
  private basePath = '/profilePic';
  constructor(private firebase: AngularFireDatabase) { }
  getImages() {
    // @ts-ignore
    return this.allImages = Imagesdelatils.slice(0);
  }

  getImage(id: number) {
    return Imagesdelatils.slice(0).find(Images => Images.id == id)
  }

  getImagesFirebase(numberItems: number): any {
    return this.firebase.list(this.basePath, ref =>
      ref.limitToLast(numberItems));
  }



}
const Imagesdelatils = [
  { "id": 1, "brand": "Apple", "url": "assets/dist/img/avatar.png" },
  { "id": 2, "brand": "Apple", "url": "assets/dist/img/avatar2.png" },
  { "id": 3, "brand": "Apple", "url": "assets/dist/img/photo1.png" },
  { "id": 4, "brand": "Apple", "url": "assets/dist/img/photo2.png" },

]
