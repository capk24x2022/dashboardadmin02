import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient,
    private firestore: AngularFirestore) {
}

getAll() {
return this.firestore.collection('drugReport').snapshotChanges();
// return this.http.get<Blog[]>(baseUrl);
}

get(id: any) {
// return this.http.get(`${baseUrl}/detail/${id}`);
return this.firestore.collection('drugReport').doc(id).get();
}
delete(id: any){
  return this.firestore.collection('drugReport').doc(id).delete();
}

}
