import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AngularFirestore} from "@angular/fire/compat/firestore";
import {Blog} from "../models/blog.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,
              private firestore: AngularFirestore) {
  }

  getAll() {
    return this.firestore.collection('users').snapshotChanges();
    // return this.http.get<Blog[]>(baseUrl);
  }

  get(id: any) {
    // return this.http.get(`${baseUrl}/detail/${id}`);
    return this.firestore.collection('users').doc(id).get();
  }



  update(id: string, blog: Blog){
    return this.firestore.collection('users').doc(id).update(blog);
    // return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any){
    return this.firestore.collection('users').doc(id).delete();
  }

}
