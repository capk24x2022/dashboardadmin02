import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Blog} from "../models/blog.model";
import {AngularFirestore} from "@angular/fire/compat/firestore";

const baseUrl = environment.baseUrl + 'api/blogs';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(private http: HttpClient,
              private firestore: AngularFirestore) {
  }

  getAll() {
    return this.firestore.collection('posts').snapshotChanges();
    // return this.http.get<Blog[]>(baseUrl);
  }

  get(id: any) {
    // return this.http.get(`${baseUrl}/detail/${id}`);
    return this.firestore.collection('posts').doc(id).get();
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);

  }

  update(id: string, blog: Blog){
    return this.firestore.collection('posts').doc(id).update(blog);
    // return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id: any){
    return this.firestore.collection('posts').doc(id).delete();
  }

  deleteAll(): Observable<any> {
    return this.http.delete(baseUrl);
  }
}
